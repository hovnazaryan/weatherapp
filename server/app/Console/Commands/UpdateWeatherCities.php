<?php

namespace App\Console\Commands;

use App\Jobs\UpdateWeatherForecast;
use App\Models\City;
use App\Models\GuestCity;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Queue;

class UpdateWeatherCities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:weather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating Weather information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cities = City::query()
            ->select('cities.id', 'cities.city_id')
            ->distinct()
            ->rightJoin('guest_cities', 'guest_cities.city_id', '=', 'cities.id')
            ->get();

        foreach ($cities as $key => $city) {
            Queue::later((5 * ($key + 1)), new UpdateWeatherForecast($city));
        }
    }
}
