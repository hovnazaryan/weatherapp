<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function guest()
    {
        return $this->belongsToMany(Guest::class, 'guest_cities')->withTimestamps();
    }

    /**
     * @param Builder $query
     */
    public function scopeLabel(Builder$query)
    {
        $query->selectRaw('CONCAT(cities.name, " ", "(", cities.country, ")") as label');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function forecasts()
    {
        return $this->hasMany(Weather::class, 'city_id', 'id')
            ->where('forecast_from', '>=', Carbon::now()->setTimezone('UTC')->startOfDay())
            ->orderBy('forecast_from', 'ASC');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function currentWeather()
    {
        return $this->hasOne(Weather::class, 'city_id', 'id')
            ->where('forecast_from', '<=', Carbon::now()->setTimezone('UTC'))
            ->where('forecast_to', '>=', Carbon::now()->setTimezone('UTC'))
            ->orderBy('forecast_from', 'ASC');
    }
}
