<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    protected $fillable = [
        'forecast_from', 'forecast_to',
        'weather_condition', 'weather_icon',
        'precipitation_unit', 'precipitation_value', 'precipitation_type',
        'wind_deg', 'wind_code', 'wind_name', 'wind_speed_mps', 'wind_speed_name',
        'temperature_unit', 'temperature', 'temperature_min', 'temperature_max',
        'pressure_unit', 'pressure',
        'humidity', 'humidity_unit',
        'clouds_name', 'clouds', 'clouds_unit',
        'sun_rise', 'sun_set', 'city_id'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    /**
     * @param $value
     * @return string
     * @throws \Exception
     */
    public function getCreatedAtAttribute($value)
    {
        $date = new Carbon($value, 'UTC');
        return $date->toIso8601String();
    }
}
