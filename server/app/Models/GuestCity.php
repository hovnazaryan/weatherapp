<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuestCity extends Model
{
    protected $hidden = [
        'guest_id', 'city_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function guest()
    {
        return $this->belongsTo(Guest::class);
    }
}
