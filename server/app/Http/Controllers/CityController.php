<?php

namespace App\Http\Controllers;

use App\Helpers\OpenWeather;
use App\Http\Requests\CityStoreRequest;
use App\Models\City;
use App\Models\Guest;
use App\Models\GuestCity;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;


class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index()
    {
        $cities = Guest::query()
            ->select('id')
            ->with(['cities' => function(HasMany $query) {
                $query
                    ->select('city_id', 'guest_id')
                    ->with(['city' => function(BelongsTo $query) {
                        $query
                            ->select('id as value', 'id')
                            ->label()
                            ->with(['currentWeather', 'forecasts']);
                    }]);
            }])
            ->where('id', '=', Auth::id())
            ->first();


        $cities = $cities->cities->map(function($item) {
            return [
                'label' => $item->city->label,
                'value' => $item->city->value,
                'current_weather' => $item->city->currentWeather ?? [],
                'forecasts' => $item->city->forecasts ?? [],
            ];
        });

        return response()->json(['cities' => $cities]);
    }

    /**
     * Display a sugesttions listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function suggestions(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'nullable|string|max:80'
        ]);

        /** @var Collection $suggestions */
        $suggestions = City::query()
            ->select('cities.id as value')
            ->label()
            ->where('name', 'LIKE', "{$request->input('keyword', '')}%")
            ->groupBy('cities.name')
            ->limit(20)
            ->get();

        return response()->json(['cities' => $suggestions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(CityStoreRequest $request)
    {
        /** @var City $city */
        $city = City::query()->findOrFail($request->input('value'));
        $owObj = new OpenWeather();
        $data = $owObj->getForecastByCityId($city->city_id);

        $city->guest()->sync([Auth::id()], false);

        return response()->json(['city' => $data], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $this->validate($request, [
            'id' => 'required|integer|exists:guest_cities,city_id'
        ]);

        $city = GuestCity::query()
            ->where('guest_id', Auth::id())
            ->where('city_id', '=', $id)
            ->firstOrFail();

        try {
            $city->delete();
            return response('', 204);
        } catch (\Exception $e) {
            return response('Error in deleting', 500);
        }
    }
}
