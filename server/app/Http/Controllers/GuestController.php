<?php

namespace App\Http\Controllers;

use App\models\Guest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class GuestController extends Controller
{
    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function authenticate()
    {
        $base64Token = base64_encode(Str::random(194));
        Guest::query()->create(['token' => $base64Token]);
        return response()->json(['token' => $base64Token]);
    }
}
