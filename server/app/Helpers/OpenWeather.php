<?php

namespace App\Helpers;

use App\Console\Commands\UpdateWeatherCities;
use App\Events\UpdatedWeather;
use App\Jobs\UpdateCityWeather;
use App\Models\City;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;

class OpenWeather
{
    /** @var null|string $_key */
    private $_key = null;

    /** @var null|string */
    private $_url = null;

    /** @var null|string */
    private $_units = null;

    /** @var null|string */
    private $_mode = null;

    /** @var string */
    private $_tableName = 'weathers';

    /**
     * OpenWeather constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        // set openweather configurations
        $this->setKey(config('services.openweather.api_key'));
        $this->setUrl(config('services.openweather.api_url'));
        $this->setUnits(config('services.openweather.units'));
        $this->setMode(config('services.openweather.mode'));
    }

    /**
     * @param integer $city_id
     * @return City
     * @throws \Exception
     */
    public function getForecastByCityId($city_id) : City
    {
        $city = $this->getCityDBbyID($city_id);

        if($city->forecasts_count) return $city;

        $res = $this->getResponse(['id' => $city_id]);
        $this->populateDB($city->id, $res);

        return $this->getCityDBbyID($city_id);
    }


    /**
     * @param $id
     * @return mixed|City
     */
    private function getCityDBbyID($id): City
    {
        return City::query()
            ->selectRaw('id as value, id')
            ->label()
            ->with(['forecasts', 'currentWeather'])
            ->withCount('forecasts')
            ->where('city_id', '=', $id)
            ->first();
    }

    /**
     * @param City $city
     * @throws \Exception
     */
    public function updateDB($city): void
    {
        $res = $this->getResponse(['id' => $city->city_id]);
        $this->populateDB($city->id, $res);

        event(new UpdatedWeather($city->id));
    }


    /**
     * @param mixed $array
     * @return array|\SimpleXMLElement
     */
    private function getResponse($array)
    {
        $client = new \GuzzleHttp\Client();
        $params = array_merge([
            'mode' => $this->_mode,
            'units' => $this->_units,
            'appid' => $this->_key,
        ], $array);

        $res = $client->get($this->_url . '?' . http_build_query($params));

        $body = $res->getBody();

        if (strtolower($this->_mode) === 'xml') {
            $xml = $body->getContents();
            return new \SimpleXMLElement($xml);
        }

        return json_decode($body, true);
    }


    /**
     * @param integer $cityID
     * @param \SimpleXMLElement|array $data
     * @throws \Exception
     */
    private function populateDB($cityID, $data): void
    {
        /** @var array $forecasts */
        $forecasts = $data->forecast->time ?? [];
        $forecastFrom = new Carbon($forecasts[0]['from'], 'UTC') ?? date('Y-m-d H:i:s', '-1');

        DB::table($this->_tableName)
            ->where('city_id', '=', $cityID)
            ->where('forecast_from', '>=', $forecastFrom)
            ->delete();


        $dataForInserting = [];
        $countArray = count($forecasts);

        for ($i = 0; $i < $countArray; $i++) {

            $dataForInserting[] = [
                'forecast_from'     => new Carbon($forecasts[$i]->attributes()->{'from'}, 'UTC'),
                'forecast_to'       => new Carbon($forecasts[$i]->attributes()->{'to'}, 'UTC') ,
                'weather_condition' => $this->checkValueAttribute($forecasts[$i]->symbol->attributes()->{'name'}),
                'weather_icon'      => $this->checkValueAttribute($forecasts[$i]->symbol->attributes()->{'var'}),
                'precipitation_unit'    => $this->checkValueAttribute($forecasts[$i]->precipitation->attributes()->{'unit'}),
                'precipitation_value'   => $this->checkValueAttribute($forecasts[$i]->precipitation->attributes()->{'value'}),
                'precipitation_type'    => $this->checkValueAttribute($forecasts[$i]->precipitation->attributes()->{'type'}),
                'wind_deg'  => $this->checkValueAttribute($forecasts[$i]->windDirection->attributes()->{'deg'}),
                'wind_code' => $this->checkValueAttribute($forecasts[$i]->windDirection->attributes()->{'code'}),
                'wind_name' => $this->checkValueAttribute($forecasts[$i]->windDirection->attributes()->{'name'}),
                'wind_speed_mps'    => $this->checkValueAttribute($forecasts[$i]->windSpeed->attributes()->{'mps'}),
                'wind_speed_name'   => $this->checkValueAttribute($forecasts[$i]->windSpeed->attributes()->{'name'}),
                'temperature_unit'  => $this->checkValueAttribute($forecasts[$i]->temperature->attributes()->{'unit'}),
                'temperature'       => $this->checkValueAttribute($forecasts[$i]->temperature->attributes()->{'value'}),
                'temperature_min'   => $this->checkValueAttribute($forecasts[$i]->temperature->attributes()->{'min'}),
                'temperature_max'   => $this->checkValueAttribute($forecasts[$i]->temperature->attributes()->{'max'}),
                'pressure_unit'     => $this->checkValueAttribute($forecasts[$i]->pressure->attributes()->{'unit'}),
                'pressure'          => $this->checkValueAttribute($forecasts[$i]->pressure->attributes()->{'value'}),
                'humidity'          => $this->checkValueAttribute($forecasts[$i]->humidity->attributes()->{'value'}),
                'humidity_unit'     => $this->checkValueAttribute($forecasts[$i]->humidity->attributes()->{'unit'}),
                'clouds_name'       => $this->checkValueAttribute($forecasts[$i]->clouds->attributes()->{'value'}),
                'clouds'            => $this->checkValueAttribute($forecasts[$i]->clouds->attributes()->{'all'}),
                'clouds_unit'       => $this->checkValueAttribute($forecasts[$i]->clouds->attributes()->{'unit'}),
                'sun_rise'          => new Carbon($data->sun->attributes()->{'rise'}, 'UTC'),
                'sun_set'           => new Carbon($data->sun->attributes()->{'set'}, 'UTC'),
                'city_id'           => $cityID ?? null,
                'created_at'        => new Carbon(),
                'updated_at'        => new Carbon()
            ];

        }

        DB::table($this->_tableName)->insert($dataForInserting);
    }


    /**
     * @param string $value
     * @return string|null
     */
    private function checkValueAttribute($value)
    {
        return !empty($value) ? (string) $value : null;
    }

    /**
     * @param string|null $units
     * @throws \Exception
     */
    public function setUnits($units)
    {
        if (empty($units)) throw new \Exception('Units is not provided');
        $this->_units = $units;
    }

    /**
     * @param string|null $url
     * @throws \Exception
     */
    public function setUrl($url)
    {
        if (empty($url)) throw new \Exception('API Url is not provided');
        $this->_url = $url;
    }

    /**
     * @param string|null $key
     * @throws \Exception
     */
    public function setKey($key)
    {
        if (empty($key)) throw new \Exception('API Key is not provided');
        $this->_key = $key;
    }

    /**
     * @param string|null $mode
     * @throws \Exception
     */
    public function setMode($mode)
    {
        if (empty($mode)) throw new \Exception('Mode is not provided');
        else if (strtolower($mode) !== 'xml') throw new \Exception('Mode for now only supported xml');

        $this->_mode = $mode;
    }
}
