<?php

namespace App\Jobs;

use App\Helpers\OpenWeather;
use App\Models\City;
use App\Models\GuestCity;
use Illuminate\Support\Facades\Queue;

class UpdateWeatherForecast extends Job
{
    /** @var City */
    protected $_city;

    /**
     * Create a new job instance.
     *
     * @param City $city
     */
    public function __construct($city)
    {
        $this->_city = $city;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $ow = new OpenWeather();
        $ow->updateDB($this->_city);
    }
}
