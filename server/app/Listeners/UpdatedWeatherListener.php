<?php

namespace App\Listeners;

use App\Events\ExampleEvent;
use App\Events\UpdatedWeather;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdatedWeatherListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent  $event
     * @return void
     */
    public function handle(UpdatedWeather $event)
    {

    }
}
