<?php

namespace App\Events;

use App\Models\City;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class UpdatedWeather extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /** @var City $city */
    public $city;

    /** @var integer */
    public $city_id;

    /**
     * Create a new event instance.
     *
     * @param integer $city_id
     */
    public function __construct($city_id)
    {
        $city = City::query()
            ->selectRaw('id as value, id')
            ->label()
            ->with(['forecasts', 'currentWeather'])
            ->where('id', '=', $city_id)
            ->first()
            ->toArray();

        $this->city = $city;
        $this->city_id = $city_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|\Illuminate\Broadcasting\Channel[]
     */
    public function broadcastOn()
    {
        return new PrivateChannel('city.' . $this->city_id);
    }
}
