<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Cities Table seeding started.');
        \Illuminate\Support\Facades\Schema::disableForeignKeyConstraints();
        \Illuminate\Support\Facades\DB::table('cities')->truncate();

        $csv = resource_path('dumps/cities.csv');

        $query = sprintf("LOAD DATA local INFILE '%s' INTO TABLE cities 
                                 FIELDS TERMINATED BY ',' 
                                 OPTIONALLY ENCLOSED BY '\"' 
                                 ESCAPED BY '\"' 
                                 LINES TERMINATED BY '\\r\\n' 
                                 IGNORE 0 LINES 
                                 (`id`, `city_id`, `name`, `country`, `long`, `lat`)", addslashes($csv));

        DB::connection()->getpdo()->exec($query);

        \Illuminate\Support\Facades\Schema::enableForeignKeyConstraints();
        $this->command->info('Cities Table seeding completed successfully.');
    }
}
