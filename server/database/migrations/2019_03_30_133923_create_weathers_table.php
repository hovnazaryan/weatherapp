<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeathersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weathers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->dateTime('forecast_from')->nullable();
            $table->dateTime('forecast_to')->nullable();

            $table->string('weather_condition', 50)->nullable();
            $table->string('weather_icon', 20)->nullable();

            $table->string('precipitation_unit', 20)->nullable();
            $table->string('precipitation_value', 20)->nullable();
            $table->string('precipitation_type', 20)->nullable();


            $table->float('wind_deg')->nullable();
            $table->string('wind_code', 20)->nullable();
            $table->string('wind_name', 40)->nullable();
            $table->float('wind_speed_mps')->nullable();
            $table->string('wind_speed_name', 40)->nullable();


            $table->string('temperature_unit', 40)->nullable();
            $table->float('temperature')->nullable();
            $table->float('temperature_min')->nullable();
            $table->float('temperature_max')->nullable();

            $table->string('pressure_unit', 40)->nullable();
            $table->float('pressure')->nullable();

            $table->float('humidity')->nullable();
            $table->string('humidity_unit', 10)->nullable();

            $table->string('clouds_name', 50)->nullable();
            $table->float('clouds')->nullable();
            $table->string('clouds_unit', 10)->nullable();


            $table->dateTime('sun_rise')->nullable();
            $table->dateTime('sun_set')->nullable();


            $table->unsignedBigInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weathers');
    }
}
