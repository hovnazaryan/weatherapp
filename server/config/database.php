<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    'migrations' => 'migrations',


    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'mysql' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST', 'localhost'),
            'port'      => env('DB_PORT', 3306),
            'database'  => env('DB_DATABASE', 'forge'),
            'username'  => env('DB_USERNAME', 'forge'),
            'password'  => env('DB_PASSWORD', ''),
            'charset'   => env('DB_CHARSET', 'utf8'),
            'collation' => env('DB_COLLATION', 'utf8_unicode_ci'),
            'prefix'    => env('DB_PREFIX', ''),
            'timezone'  => env('DB_TIMEZONE', '+00:00'),
            'strict'    => env('DB_STRICT_MODE', false),
            'options' => [PDO::MYSQL_ATTR_LOCAL_INFILE => true]
        ],

        'mysql_testing' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST_TESTING', 'localhost'),
            'port'      => env('DB_PORT_TESTING', 3306),
            'database'  => env('DB_DATABASE_TESTING', 'forge'),
            'username'  => env('DB_USERNAME_TESTING', 'forge'),
            'password'  => env('DB_PASSWORD_TESTING', ''),
            'charset'   => env('DB_CHARSET_TESTING', 'utf8'),
            'collation' => env('DB_COLLATION_TESTING', 'utf8_unicode_ci'),
            'prefix'    => env('DB_PREFIX_TESTING', ''),
            'timezone'  => env('DB_TIMEZONE_TESTING', '+00:00'),
            'strict'    => env('DB_STRICT_MODE_TESTING', false),
            'options' => [PDO::MYSQL_ATTR_LOCAL_INFILE => true]
        ]
    ],

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DB', 0),
        ],

    ],

];
