<?php

return [
    'openweather' => [
        'api_url' => env('OW_API_URL', null),
        'api_key' => env('OW_API_KEY', null),
        'units'   => env('OW_UNITS', 'metric'),
        'mode'    => env('OW_MODE', 'xml'),
    ]
];
