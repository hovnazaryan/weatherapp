<?php

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        putenv('DB_DEFAULT=mysql_testing');

        return require __DIR__.'/../bootstrap/app.php';
    }
}
