<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CityTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCitiesAllUnauthorized()
    {
        $response = $this->json('GET', 'cities');
        $response->assertResponseStatus(401);
        $response->seeJsonEquals(["message" => 'Unauthorized']);
    }


    public function testCitiesAllAuthorized()
    {
        $auth = $this->post('auth');
        $auth->seeJsonStructure(['token']);

        $token = json_decode($auth->response->getContent(), true)['token'];

        $response = $this->json('GET', 'cities', [], [
            'Authorization' => $token
        ]);

        $response->seeJsonStructure(['cities' => [
            '*' => [
                'current_weather',
                'forecasts',
                'label',
                'value'
            ]
        ]]);
    }


    public function testCitiesSuggestionWithoutKeyword()
    {
        $auth = $this->post('auth');
        $auth->seeJsonStructure(['token']);

        $token = json_decode($auth->response->getContent(), true)['token'];

        $response = $this->json('GET', 'cities/suggestions', [], [
            'Authorization' => $token
        ]);

        $response->seeJsonStructure(['cities' => [
            '*' => [
                'label',
                'value'
            ]
        ]]);
    }


    public function testCitiesSuggestionWithValidKeyword()
    {
        $auth = $this->post('auth');
        $auth->seeJsonStructure(['token']);
        $auth->assertResponseStatus(200);

        $token = json_decode($auth->response->getContent(), true)['token'];

        $response = $this->json('GET', 'cities/suggestions', ['keyword' => 'Yerevan'], [
            'Authorization' => $token
        ]);
        $auth->assertResponseStatus(200);

        $response->seeJsonStructure(['cities' => [
            '*' => [
                'label',
                'value'
            ]
        ]]);
    }


    public function testCitiesSuggestionWithInvalidKeyword()
    {
        $auth = $this->post('auth');
        $auth->seeJsonStructure(['token']);

        $token = json_decode($auth->response->getContent(), true)['token'];

        $response = $this->json('GET', 'cities/suggestions', ['keyword' => \Illuminate\Support\Str::random(10000)], [
            'Authorization' => $token
        ]);
        $auth->assertResponseStatus(422);

        $response->seeJsonEquals([
            'keyword' => ["The keyword may not be greater than 80 characters."]
        ]);
    }


    public function testCitiesCreateIncorrectData()
    {
        $auth = $this->post('auth');
        $auth->seeJsonStructure(['token']);

        $token = json_decode($auth->response->getContent(), true)['token'];

        $response = $this->json('POST', 'cities', ['value' => 'test'], [
            'Authorization' => $token
        ]);

        $auth->assertResponseStatus(422);

        $response->seeJsonEquals([
            'value' => [
                "The value must be an integer."
            ]
        ]);

        $response = $this->json('POST', 'cities', ['value' => 0], [
            'Authorization' => $token
        ]);

        $auth->assertResponseStatus(422);

        $response->seeJsonEquals([
            'value' => [
                "The selected value is invalid."
            ]
        ]);

        $response = $this->json('POST', 'cities', [], [
            'Authorization' => $token
        ]);

        $auth->assertResponseStatus(422);

        $response->seeJsonEquals([
            'value' => [
                "The value field is required."
            ]
        ]);
    }

    public function testCitiesCreateCorrectData()
    {
        $auth = $this->post('auth');
        $auth->seeJsonStructure(['token']);

        $token = json_decode($auth->response->getContent(), true)['token'];

        $response = $this->json('POST', 'cities', ['value' => \App\Models\City::query()->first()->id], [
            'Authorization' => $token
        ]);

        $response->seeStatusCode(201);
    }


    public function testCityDestroy()
    {
        $auth = $this->post('auth');
        $auth->seeJsonStructure(['token']);

        $token = json_decode($auth->response->getContent(), true)['token'];

        $response = $this->json('DELETE', 'cities/2', ['value' => \App\Models\City::query()->first()->id], [
            'Authorization' => $token
        ]);

        $response->seeJsonEquals([
            'id' => [
                "The selected id is invalid."
            ]
        ]);

        $response->seeStatusCode(422);


        $response = $this->json('DELETE', 'cities/test', ['value' => \App\Models\City::query()->first()->id], [
            'Authorization' => $token
        ]);

        $response->seeJsonEquals([
            'id' => [
                "The id must be an integer."
            ]
        ]);

        $response->seeStatusCode(422);


        $id = \App\Models\City::query()->first()->id;

        $this->json('POST', 'cities', ['value' => $id], [
            'Authorization' => $token
        ]);

        $response = $this->json('DELETE', "cities/$id", [], [
            'Authorization' => $token
        ]);

        $response->seeStatusCode(204);
    }
}
