<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class GuestTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testAuthenticate()
    {
        $response = $this->post('auth');
        $response->assertResponseStatus(200);
        $response->seeJsonStructure(["token"]);
    }
}
