<?php


$router->post('auth', 'GuestController@authenticate');

$router->group(['middleware' => 'auth'], function ($app) {
    $app->group(['prefix' => 'cities'], function ($cities) {
        $cities->get('/', 'CityController@index');
        $cities->get('suggestions', 'CityController@suggestions');
        $cities->post('/', 'CityController@store');
        $cities->delete('{id}', 'CityController@destroy');
    });
});
