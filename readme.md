#Weather Task APP
The weather APP developed with Vue.js and Lumen also used Redis and Socket.io 

##OpenWeather
We using [OpenWeather](https://openweathermap.org/forecast5) `Call 5 day / 3 hour forecast data` 
API endpoint we used for now only xml mode and temperature with celsius


##Installation API
1. cd server 
2. composer install
3. create new MySql DB
4. copy .env.example to .env and configure
5. for getting OpenWeather API key visit [here](https://openweathermap.org)
6. `php artisan key:generate`
7. `php artisan migrate --seed`
8. for running API you can use `php artisan server` instead of creating v-host
**each .env change need to rerun this command**
    
##Installation Front
1. cd client
2. npm install
3. copy .env.example to .env
4. for run socket write on cmd `node socket.js` on client folder by default socket port is `6001` 
please make sure redis is run successfully  before run this command
5. `VUE_APP_API_URL` API URL (LUMEN)
6. `VUE_APP_SOCKET_HOST` Socket HOST  
7. for running dev mode please run this command `npm run serve` will be run node server 
8. for prod mode `npm run build` generating dist folder with compiled js,scss etc..

## Real time update
For real time updating as I mentioned before we using Socket.io and Redis
1. make sure on server/.env file you set `QUEUE_DRIVER=redis` and `BROADCAST_DRIVER=redis`
2. make sure you have ran `node socket.js` on client folder
3. make sure redis is working correctly
4. ran `php artisan queue:listen` for running queue listener  
5. and for getting updates weather need to run `php artisan schedule:run` 
need to add on cron job for running hourly (hourly getting new date updates) 
and after getting updated data it will broadcast with redis and on Front-End 
data will updating automatically 

##Configuring REDIS [Link](https://redis.io/) 
1. on .env you can find `REDIS_HOST, REDIS_PASSWORD, REDIS_PORT` 
2. you can test by run redis-cli and write on cmd PING if returns PONG then redis is ran successfully

    
##TESTING
1. Create New MySql 
2. .env set configurations
    `DB_CONNECTION_TESTING,
    DB_HOST_TESTING,
    DB_PORT_TESTING,
    DB_DATABASE_TESTING,
    DB_USERNAME_TESTING,
    DB_PASSWORD_TESTING`
3. open phpunit.xml config file `<env>` and change configuration
4. `php artisan migrate --database="mysql_testing" --seed`
5. for running test `./vendor/bin/phpunit`