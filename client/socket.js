const app = require('http').createServer(handler);
const io  = require('socket.io')(app);

const Redis = require('ioredis');
const redis = new Redis();

app.listen(6001, function() {
    console.log('Server is running on PORT :6001!');

    io.on('connection', function(socket) {
        console.log('Connected successfully.');
    });

    redis.psubscribe('*', function(err, count) {
        console.log('Subscribed!');
    });

    redis.on('pmessage', function(subscribed, channel, message) {
        message = JSON.parse(message);
        io.emit('city.' + message.data.city_id, message.data.city);
    });
});

function handler(req, res) {
    res.writeHead(200);
    res.end('');
}