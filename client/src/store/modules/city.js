import {RepositoryFactory} from '../../api/RepositoryFactory';
import io from 'socket.io-client';

const CitiesRepository = RepositoryFactory.get('cities');

const socket = io(process.env.VUE_APP_SOCKET_HOST);

export default {
    state: {
        city  : {},
        cities: [],
        loading : false,
        loadingCities : false
    },
    mutations: {
        selectCity(state, city) {
            if(city) state.city = city;
            else if(!state.cities.length) state.city = {};
            else state.city = state.cities[0];
        },
        addCity(state, city) {
            state.cities.push(city);
        },
        removeCity(state, key) {
            state.cities.splice(key, 1);
        },
        setCities(state, cities) {
            state.cities = cities;
        },
        setLoadingCities(state, value) {
            state.loadingCities = value;
        }
    },
    actions: {
        setLoading({commit, state}, value) {
            commit('setLoadingCities', value);
        },
        selectActiveCity({commit, state}, city) {
            commit('selectCity', city);
        },
        addNewCity({commit, state}, city) {
            let cityExist = state.cities.findIndex(item => item.value === city.value);

            if (cityExist === -1) {
                state.loading = true;
                CitiesRepository.createCity(city).then((res) => {
                    commit('addCity', res.data.city);
                    commit('selectCity');
                    state.loading = false;
                });
            }
        },
        removeCity({commit, state}, key) {
            CitiesRepository.deleteCity(state.cities[key].value);

            commit('removeCity', key);
            commit('selectCity');
        },
        populateCities({commit, state}, cities) {
            commit('setCities', cities);

            // subscribe cities private channels
            cities.map((o) => {
                socket.on(`city.${o.value}`, (data) => {
                    state.cities = state.cities.map((city) => {
                        if (city.value === data.id) return data;
                        return city;
                    });
                });
            });

            commit('selectCity', (cities[0] || undefined));
        }
    },
    getters: {
        selectedCity(state) {
            return state.city;
        },
        selectedCities(state) {
            return state.cities;
        },
        loading(state) {
            return state.loading;
        },
        loadingCities(state) {
            return state.loadingCities;
        }
    }
}