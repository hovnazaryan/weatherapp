import Repository from './Repository';

const resource = 'cities';

export default {
    get() {
        return Repository.get(`${resource}`);
    },
    suggestions (keyword) {
        return Repository.get(`${resource}/suggestions`, {params : {keyword}});
    },
    createCity(payload) {
        return Repository.post(`${resource}`, payload);
    },
    deleteCity(cityId) {
        return Repository.delete(`${resource}/${cityId}`, );
    }
}