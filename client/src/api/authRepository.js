import Repository from './Repository';

const resource = 'auth';

export default {
    getToken() {
        return Repository.post(`${resource}`);
    }
}