import CitiesRepository from './cityRepository';
import AuthRepository from './authRepository';

const repositories = {
    cities: CitiesRepository,
    auth  : AuthRepository
}

export const RepositoryFactory = {
    get : name => repositories[name]
}