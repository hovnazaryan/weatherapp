import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import './registerServiceWorker'
import axios from 'axios'
import VueAxios from 'vue-axios'
import vSelect from 'vue-select'
import mixins from './mixins/index';

require('./bootrstrap');
window.Highcharts = require('highcharts');

Vue.use(VueAxios, axios);
Vue.mixin(mixins);
Vue.component('v-select', vSelect);

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
