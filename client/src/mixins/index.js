import Vue from 'vue';
const moment = require('moment');

export default  Vue.mixin({
    data() {
        return {
            moment : moment,
            userTimezoneOffset : -((new Date().getTimezoneOffset()) / 60)
        }
    },
    computed : {
        CITY : function () {
            return this.$store.getters.selectedCity || {};
        },
        CITIES : function () {
            return this.$store.getters.selectedCities || [];
        },
        WEATHER_CUR : function () {
            return this.CITY.current_weather || {};
        },
        IS_SELECTED_CITY : function () {
            return this.CITY.label;
        },
        FORECASTS_CITY : function () {
            return this.CITY.forecasts || {};
        },
        LOADING : function () {
            return this.$store.getters.loading;
        },
        LOADING_CITIES : function () {
            return this.$store.getters.loadingCities;
        },
    }
});